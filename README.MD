# TP1: Trouver le bon nombre

1. Générer un nombre aléatoire entre 0 et 100 et le stocker dans une variable "nombreADeviner".
2. Initialiser une variable "tentatives" à 0.
3. Tant que la réponse de l'utilisateur n'est pas égale à "nombreADeviner", répéter les étapes suivantes :
    a. Demander à l'utilisateur de saisir un nombre entre 0 et 100.
    b. Incrémenter "tentatives" de 1.
    c. Si la réponse de l'utilisateur est plus petite que "nombreADeviner", afficher "Plus grand".
    d. Si la réponse de l'utilisateur est plus grande que "nombreADeviner", afficher "Plus petit".
4. Afficher "Gagné, vous avez trouvé le bon nombre [nombreADeviner]. Vous avez fait [tentatives] tentatives."



# TP2: Trouver le bon nombre (Joueur contre Ordinateur)

1. Générez un nombre aléatoire entre 1 et 100 et stockez-le dans "nombreSecret".
2. Initialisez un compteur de tentatives à zéro.

3. Début du jeu :
   a. L'ordinateur génère un nombre aléatoire entre 1 et 100 et le stocke dans "guessOrdinateur".
   b. L'utilisateur devine le nombre et le saisit.
   c. Incrémentez le compteur de tentatives de 1.

   d. Comparez le nombre de l'utilisateur avec "nombreSecret" :
      - Si l'utilisateur a deviné correctement, affichez "Bravo ! Vous avez gagné en [compteur de tentatives] tentatives" et quittez le jeu.
      - Si le nombre de l'utilisateur est plus petit, affichez "Le nombre est plus grand. Essayez à nouveau."
      - Sinon, affichez "Le nombre est plus petit. Essayez à nouveau."

   e. L'ordinateur ajuste son prochain "guessOrdinateur" en fonction des indices de l'utilisateur.

4. Fin du jeu.



# TP3: Chiffrement CESAR

1. Demander à l'utilisateur de choisir un pas pour le chiffrement (entre 1 et 9).
2. Demander à l'utilisateur de saisir un texte à chiffrer.
3. Chiffrer le texte en appliquant un décalage du pas choisi.
4. Afficher le texte chiffré.
5. Demander à l'utilisateur de spécifier le pas de déchiffrement.
6. Déchiffrer le texte chiffré en appliquant le décalage inverse.
7. Afficher le texte déchiffré.

8. Fin du processus.



# TP4: Jeu des bâtonnets

1. Démarrez le jeu avec 20 bâtonnets.
2. Les joueurs (l'ordinateur et l'humain) se relaient pour retirer entre 1 et 3 bâtonnets à chaque tour.
3. Le joueur qui retire le dernier bâtonnet perd la partie.
4. Affichez "Vous avez gagné" si l'humain gagne, sinon affichez "L'ordinateur a gagné".

5. Les règles sont simples : les joueurs prennent tour à tour entre 1 et 3 bâtonnets.
6. Le joueur gagnant est celui qui force l'adversaire à prendre le dernier bâtonnet.

7. Le jeu se termine dès qu'un des joueurs retire le dernier bâtonnet.
8. Le jeu affiche le gagnant.

9. Fin de la partie.

# TP5 : Jeu du pendu

1. Sélectionnez un mot à deviner et stockez-le dans une variable "motADeviner".
2. Transformez le mot en un tableau de lettres et stockez-le dans une variable "lettresMot".
3. Initialisez une variable "motDevine" sous forme de tableau avec autant d'éléments que de lettres dans "lettresMot", chaque élément initialisé à "_".
4. Fixez le nombre de tentatives à 10.

5. Début du jeu :
   a. Affichez "Bienvenue dans le jeu de deviner le mot !".
   b. Tant que le nombre de tentatives est supérieur à 0, répétez les étapes suivantes :
      i.   Affichez le "motDevine" actuel, en remplaçant les "_"" par des lettres déjà devinées.
      ii.  Affichez le nombre de tentatives restantes.
      iii. Demandez au joueur de saisir une lettre et stockez-la dans une variable "lettre".
      iv.  Si la lettre saisie n'est pas une seule lettre valide, affichez "Veuillez entrer une seule lettre valide" et continuez la boucle.
      v.   Vérifiez si la "lettre" existe dans le "motADeviner" et mettez à jour le "motDevine" en remplaçant les "_" par la "lettre" trouvée, si elle existe.
      vi.  Si la "lettre" n'existe pas, décrémentez le nombre de tentatives.
      vii. Si "motDevine" est égal à "motADeviner", affichez "Bravo ! Vous avez gagné." et sortez du jeu.

6. Si le nombre de tentatives atteint 0, affichez "Désolé, vous avez perdu. Le mot était [motADeviner]."

7. Fin du jeu.

