<?php
// Generate a random number between 1 and 100
$randomNumber = rand(1, 100);

// Initialize variables to keep track of the user and PC guesses
$userGuess = null;
$pcGuess = null;

// Main game loop
while (true) {
    // User's turn
    echo "User's turn: ";
    $userGuess = intval(fread(STDIN, 100));

    // Check if the user's guess is correct
    if ($userGuess === $randomNumber) {
        echo "Congratulations! You guessed the number $randomNumber. You win!\n";
        break;
    } elseif ($userGuess < $randomNumber) {
        echo "The number is larger than $userGuess. Try again.\n";
    } else {
        echo "The number is smaller than $userGuess. Try again.\n";
    }

    // PC's turn
    echo "PC's turn: ";
    if ($pcGuess === null) {
        // Generate a random guess for the PC
        $pcGuess = rand(1, 100);
    }
    echo $pcGuess;
    
    // Provide feedback to both the user and the PC
    if ($pcGuess === $randomNumber) {
        echo "\nPC guessed the number $randomNumber. PC wins!\n";
        break;
    } elseif ($pcGuess < $randomNumber) {
        echo " (The number is larger.)\n";
    } else {
        echo " (The number is smaller.)\n";
    }

    // Adjust PC's guess based on available information
    if ($pcGuess < $randomNumber) {
        $pcGuess = rand($pcGuess + 1, 100);
    } else {
        $pcGuess = rand(1, $pcGuess - 1);
    }
}
?>
