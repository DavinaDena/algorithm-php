<?php
function prendreBatonnets($batonnetsRestants) {
    // L'ordinateur prend un nombre de bâtonnets pour gagner
    // Si le nombre restant est une puissance de 4 (4, 16, 64, ...), alors l'ordinateur prend un nombre aléatoire entre 1 et 3.
    if (($batonnetsRestants - 1) % 4 == 0) {
        return rand(1, 3);
    }
    // Sinon, l'ordinateur prend juste de manière à ce que le nombre restant soit une puissance de 4.
    else {
        return ($batonnetsRestants - 1) % 4;
    }
}

$batonnetsRestants = 20;

while ($batonnetsRestants > 0) {
    echo "Il reste $batonnetsRestants bâtonnets. Combien voulez-vous en prendre (entre 1 et 3) ? : ";
    $choixJoueur = (int)readline();

    if ($choixJoueur < 1 || $choixJoueur > 3 || $choixJoueur > $batonnetsRestants) {
        echo "Choix invalide. Veuillez choisir entre 1 et 3 bâtonnets.\n";
    } else {
        $batonnetsRestants -= $choixJoueur;

        if ($batonnetsRestants <= 0) {
            echo "Vous avez gagné !\n";
            break;
        }

        $choixOrdinateur = prendreBatonnets($batonnetsRestants);
        echo "L'ordinateur prend $choixOrdinateur bâtonnet(s).\n";
        $batonnetsRestants -= $choixOrdinateur;

        if ($batonnetsRestants <= 0) {
            echo "L'ordinateur a gagné !\n";
        }
    }
}
?>
