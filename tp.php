<?php
$nombreADeviner = rand(0, 100);
$tentatives = 0;

while (true) {
    echo "Devinez le nombre (entre 0 et 100) : ";
    $reponseUtilisateur = (int)readline();
    
    $tentatives++;

    if ($reponseUtilisateur < $nombreADeviner) {
        echo "C'est plus grand." . PHP_EOL;
    } elseif ($reponseUtilisateur > $nombreADeviner) {
        echo "C'est plus petit." . PHP_EOL;
    } else {
        echo "Félicitations ! Vous avez trouvé le nombre $nombreADeviner en $tentatives tentatives." . PHP_EOL;
        break;
    }
}
?>
