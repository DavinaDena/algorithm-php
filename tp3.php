<?php
function chiffrementCesar($texte, $pas) {
    $texteChiffre = "";
    $longueurAlphabet = 26;

    for ($i = 0; $i < strlen($texte); $i++) {
        $lettre = $texte[$i];
        
        if (ctype_alpha($lettre)) {
            $majuscules = ctype_upper($lettre);
            $lettre = strtolower($lettre);
            $lettreChiffree = chr((ord($lettre) - ord('a') + $pas) % $longueurAlphabet + ord('a'));
            
            if ($majuscules) {
                $lettreChiffree = strtoupper($lettreChiffree);
            }

            $texteChiffre .= $lettreChiffree;
        } else {
            $texteChiffre .= $lettre;
        }
    }

    return $texteChiffre;
}

function dechiffrementCesar($texteChiffre, $pas) {
    return chiffrementCesar($texteChiffre, -$pas);
}

echo "Entrez le texte à chiffrer : ";
$texte = readline();
echo "Entrez le pas de chiffrement (entre 1 et 9) : ";
$pas = intval(readline());

$texteChiffre = chiffrementCesar($texte, $pas);
echo "Texte chiffré : $texteChiffre\n";

echo "Entrez le pas de déchiffrement : ";
$pasDechiffrement = intval(readline());

$texteDechiffre = dechiffrementCesar($texteChiffre, $pasDechiffrement);
echo "Texte déchiffré : $texteDechiffre\n";
?>
