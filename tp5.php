<?php
// Mot à deviner (vous pouvez le changer)
$motADeviner = "informatique";

// Transformer le mot en tableau de lettres
$lettresMot = str_split($motADeviner);

// Initialisation des variables
$motDevine = array_fill(0, count($lettresMot), "_");
$tentatives = 10;

echo "Bienvenue dans le jeu de deviner le mot !\n";

while ($tentatives > 0) {
    echo "Mot actuel: " . implode(" ", $motDevine) . "\n";
    echo "Tentatives restantes: $tentatives\n";
    
    echo "Entrez une lettre : ";
    $lettre = strtolower(trim(readline()));

    if (strlen($lettre) != 1 || !ctype_alpha($lettre)) {
        echo "Veuillez entrer une seule lettre valide.\n";
        continue;
    }

    $lettreTrouvee = false;
    
    for ($i = 0; $i < count($lettresMot); $i++) {
        if ($lettresMot[$i] == $lettre) {
            $motDevine[$i] = $lettre;
            $lettreTrouvee = true;
        }
    }

    if (!$lettreTrouvee) {
        $tentatives--;
    }

    if (implode("", $motDevine) == $motADeviner) {
        echo "Bravo ! Vous avez gagné. Le mot était '$motADeviner'.\n";
        break;
    }
}

if ($tentatives == 0) {
    echo "Désolé, vous avez perdu. Le mot était '$motADeviner'.\n";
}
?>
